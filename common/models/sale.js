module.exports = function(Sale) {
  Sale.beforeRemote('create', function(context, user, next) {
    var req = context.req;
    req.body.dateJoinedFund = Date.now();
    req.body.employeeId = req.accessToken.userId;
    next();
  });
};
