module.exports = function(Branch) {
  Branch.getLocation = function(branchId, cb) {
     Branch.findById( branchId, function (err, instance) {
         response = "Name of branch is " + instance.location;
         cb(null, response);
         console.log(response);
     });
   };

   Branch.remoteMethod (
         'getLocation',
         {
           http: {path: '/getlocation', verb: 'get'},
           accepts: {arg: 'id', type: 'number', http: { source: 'query' } },
           returns: {arg: 'location', type: 'string'}
         }
     );
};
