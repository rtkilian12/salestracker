var async = require('async');
module.exports = function(app) {
  //data sources
  var postgresIDs = app.dataSources.postgresIDs;

  //create all models
  async.parallel({
    employees: async.apply(createEmployees),
    branches: async.apply(createBranches),
  }, function(err, results) {
    if (err) throw err;
    createSales(results.employees, results.branches, function(err) {
      console.log('> models created sucessfully');
    });
  });
  //create employees
  function createEmployees(cb) {
    postgresIDs.automigrate('Employee', function(err) {
      if (err) return cb(err);
      var Employee = app.models.Employee;
      Employee.create([
        {email: 'foo@bar.com', password: 'foobar'},
        {email: 'john@doe.com', password: 'johndoe'},
        {email: 'jane@doe.com', password: 'janedoe'}
      ], cb);
    });
  }
  //create branches
  function createBranches(cb) {
    postgresIDs.automigrate('Branch', function(err) {
      if (err) return cb(err);
      var Branch = app.models.Branch;
      Branch.create([
        {location: 'Wollongong', area: 'south'},
        {location: 'Miranda', area: 'east'},
        {location: 'Parramatta', area: 'west'},
      ], cb);
    });
  }
  //create sales
  function createSales(employees, branches, cb) {
    postgresIDs.automigrate('Sale', function(err) {
      if (err) return cb(err);
      var Sale = app.models.Sale;
      var DAY_IN_MILLISECONDS = 1000 * 60 * 60 * 24;
      Sale.create([
        {
          dateProcessed: Date.now() - (DAY_IN_MILLISECONDS * 4),
          dateJoinedFun: Date.now() - (DAY_IN_MILLISECONDS * 4.2),
          policyNumber: 12345,
          saleAmount: 3423.23,
          dateFinancial: Date.now() - (DAY_IN_MILLISECONDS * 4.5),
          welcomeCall: Date.now() - (DAY_IN_MILLISECONDS * 4.7),
          commentWelcomeCall: 'Say hello',
          employeeId: employees[0].id,
          BranchId: branches[0].id,
        },
        {
          dateProcessed: Date.now() - (DAY_IN_MILLISECONDS * 3),
          dateJoinedFun: Date.now() - (DAY_IN_MILLISECONDS * 3.2),
          policyNumber: 838,
          saleAmount: 234.39,
          dateFinancial: Date.now() - (DAY_IN_MILLISECONDS * 3.5),
          welcomeCall: Date.now() - (DAY_IN_MILLISECONDS * 3.7),
          welcomeCallComment: 'Say hello 1',
          employeeId: employees[1].id,
          BranchId: branches[0].id,
        },
        {
            dateProcessed: Date.now() - (DAY_IN_MILLISECONDS * 2),
            dateJoinedFun: Date.now() - (DAY_IN_MILLISECONDS * 2.2),
            policyNumber: 5324,
            saleAmount: 834.40,
            dateFinancial: Date.now() - (DAY_IN_MILLISECONDS * 2.5),
            welcomeCall: Date.now() - (DAY_IN_MILLISECONDS * 2.7),
            welcomeCallComment: 'Say hello 2',
            employeeId: employees[1].id,
            BranchId: branches[1].id,
        },
        {
            dateProcessed: Date.now() - (DAY_IN_MILLISECONDS * 1),
            dateJoinedFun: Date.now() - (DAY_IN_MILLISECONDS * 1.2),
            policyNumber: 125435,
            saleAmount: 423.54,
            dateFinancial: Date.now() - (DAY_IN_MILLISECONDS * 1.5),
            welcomeCall: Date.now() - (DAY_IN_MILLISECONDS * 1.7),
            welcomeCallComment: 'Say hello 3',
            employeeId: employees[2].id,
            BranchId: branches[2].id,
        }
      ], cb);
    });
  }
};
