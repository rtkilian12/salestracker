angular
  .module('app')
  .controller('AllSalesController', ['$scope', 'Sale', function($scope,
      Sale) {
    $scope.sales = Sale.find({
      filter: {
        include: [
          'branch',
          'employee'
        ]
      }
    });
  }])
  .controller('AddSaleController', ['$scope', 'Branch', 'Sale',
      '$state', function($scope, Branch, Sale, $state) {
    $scope.action = 'Add';
    $scope.branches = [];
    $scope.selectedBranch;
    $scope.sale = {};
    $scope.isDisabled = false;

    Branch
      .find()
      .$promise
      .then(function(branches) {
        $scope.branches = branches;
        $scope.selectedBranch = $scope.selectedBranch || branches[0];
      });

    $scope.submitForm = function() {
      Sale
        .create({
          employeeId: $scope.employeeId.id,
          dateProcessed: $scope.sale.dateProcessed,
          dateJoinedFund: $scope.sale.dateJoinedFund,
          policyNumber: $scope.sale.policyNumber,
          saleAmount: $scope.sale.saleAmount,
          dateFinancial: $scope.sale.dateFinancial,
          welcomeCall: $scope.sale.welcomeCall,
          welcomeCallComment: $scope.sale.welcomeCallComment
        .$promise
        .then(function() {
          $state.go('all-sales');
        });
      });
    };
  }])
  .controller('DeleteSaleController', ['$scope', 'Sale', '$state',
      '$stateParams', function($scope, Sale, $state, $stateParams) {
    Sale
      .deleteById({ id: $stateParams.id })
      .$promise
      .then(function() {
        $state.go('my-sales');
      });
  }])
  .controller('EditSaleController', ['$scope', '$q', 'Branch', 'Sale',
      '$stateParams', '$state', function($scope, $q, Branch, Sale,
      $stateParams, $state) {
    $scope.action = 'Edit';
    $scope.branches = [];
    $scope.selectedBranch;
    $scope.sale = {};
    $scope.isDisabled = true;

    $q
      .all([
        Branch.find().$promise,
        Sale.findById({ id: $stateParams.id }).$promise
      ])
      .then(function(data) {
        var branches = $scope.branches = data[0];
        $scope.sale = data[1];
        $scope.selectedBranch;

        var selectedBranchIndex = branches
          .map(function(coffeeShop) {
            return branch.id;
          })
          .indexOf($scope.sale.branchId);
        $scope.selectedBranch = branches[selectedBranchIndex];
      });

    $scope.submitForm = function() {
      $scope.sale.branchId = $scope.selectedBranch.id;
      $scope.sale
        .$save()
        .then(function(sale) {
          $state.go('all-sales');
        });
    };
  }])
  .controller('MySalesController', ['$scope', 'Sale', '$rootScope',
      function($scope, Sale, $rootScope) {
    $scope.sales = Sale.find({
      filter: {
        where: {
          employeeId: $rootScope.currentUser.id
        },
        include: [
          'branch',
          'employee'
        ]
      }
    });
  }]);
