angular
  .module('app', [
    'ui.router',
    'lbServices'
  ])
  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider,
      $urlRouterProvider) {
    $stateProvider
      .state('add-sale', {
        url: '/add-sale',
        templateUrl: 'views/sale-form.html',
        controller: 'AddSaleController',
        authenticate: true
      })
      .state('all-sales', {
        url: '/all-sales',
        templateUrl: 'views/all-sales.html',
        controller: 'AllSalesController'
      })
      .state('edit-sale', {
        url: '/edit-sale/:id',
        templateUrl: 'views/sale-form.html',
        controller: 'EditSaleController',
        authenticate: true
      })
      .state('delete-sale', {
        url: '/delete-sale/:id',
        controller: 'DeleteSaleController',
        authenticate: true
      })
      .state('forbidden', {
        url: '/forbidden',
        templateUrl: 'views/forbidden.html',
      })
      .state('login', {
        url: '/login',
        templateUrl: 'views/login.html',
        controller: 'AuthLoginController'
      })
      .state('logout', {
        url: '/logout',
        controller: 'AuthLogoutController'
      })
      .state('my-sales', {
        url: '/my-sales',
        templateUrl: 'views/my-sale.html',
        controller: 'MySalesController',
        authenticate: true
      })
      .state('sign-up', {
        url: '/sign-up',
        templateUrl: 'views/sign-up-form.html',
        controller: 'SignUpController',
      })
      .state('sign-up-success', {
        url: '/sign-up/success',
        templateUrl: 'views/sign-up-success.html'
      });
    $urlRouterProvider.otherwise('all-sales');
  }])
  .run(['$rootScope', '$state', function($rootScope, $state) {
    $rootScope.$on('$stateChangeStart', function(event, next) {
      // redirect to login page if not logged in
      if (next.authenticate && !$rootScope.currentUser) {
        event.preventDefault(); //prevent current page from loading
        $state.go('forbidden');
      }
    });
  }]);
